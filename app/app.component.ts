import { Component }    from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  template: `
    <nav>
      <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
      <a routerLink="/entries" routerLinkActive="active">Entries</a>
    </nav>
    <router-outlet></router-outlet>
  `
})

export class AppComponent {

}
