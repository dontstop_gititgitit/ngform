export class Entry {
  id: number;
  zipCode: number;
  firstName: string;
  lastName: string;
}
