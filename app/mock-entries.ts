import { Entry } from './Entry/entry';

export const ENTRIES: Entry[] = [
  {
    id: 11, zipCode: 60661, firstName: "Imagination", lastName: "Publishing"
  },
  {
    id: 13, zipCode: 60657, firstName: "Brian", lastName: "Clarke"
  },
  {
    id: 15, zipCode: 60601, firstName: "Lincoln", lastName: "Square"
  },
  {
    id: 17, zipCode: 60625, firstName: "Ravenswood", lastName: "Manor"
  },
];
