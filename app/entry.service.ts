import { Entry }      from './Entry/entry';
import { ENTRIES }    from './mock-entries';
import { Injectable } from '@angular/core';

@Injectable()
export class EntryService {
  getEntries(): Promise<Entry[]> {
    return Promise.resolve(ENTRIES);
  }

  getEntriesSlowly(): Promise<Entry[]> {
    return new Promise(resolve => {
      // Simulate server latency with 2 second delay
      setTimeout(() => resolve(this.getEntries()), 2000);
    });
  }

  getEntry(id: number): Promise<Entry> {
    return this.getEntries()
               .then(entries => entries.find(entry => entry.id === id));
  }
}
