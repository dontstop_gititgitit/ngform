import { NgModule }      from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

import { EntryDetailComponent } from './EntryDetailComponent/entry-detail.component';
import { EntriesComponent } from './EntriesComponent/entries.component';
import { DashboardComponent } from './DashboardComponent/dashboard.component';

const ROUTES: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'entry/:id', component: EntryDetailComponent },
  { path: 'entries',   component: EntriesComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
