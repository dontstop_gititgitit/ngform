import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Entry } from '../Entry/entry';
import { EntryService } from '../entry.service';

@Component({
  moduleId: module.id,
  selector: 'my-entries',
  templateUrl: 'entries.component.html'
})

export class EntriesComponent implements OnInit {
  entry: Entry;
  entries: Entry[];
  selectedEntry: Entry;

  constructor(
    private router: Router,
    private entryService: EntryService
  ) {}

  getEntries(): void {
    this.entryService.getEntries().then(entries => this.entries = entries);
  }

  ngOnInit(): void {
    this.getEntries();
  }

  onSelect(entry: Entry): void {
    this.selectedEntry = entry;
  }

  gotoDetail(): void {
    this.router.navigate(['/entry', this.selectedEntry.id]);
  }
}
