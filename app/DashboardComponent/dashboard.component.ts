import { Component, OnInit } from '@angular/core';

import { Entry } from '../Entry/entry';
import { EntryService } from '../entry.service';

@Component({
  moduleId: module.id,
  selector: 'my-dashboard',
  templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit {

  entries: Entry[] = [];

  constructor(private entryService: EntryService) {}

  ngOnInit(): void {
    this.entryService.getEntries()
      .then(entries => this.entries = entries);
  }

}
