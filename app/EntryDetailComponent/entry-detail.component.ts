import 'rxjs/add/operator/switchMap';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

import { Entry } from '../Entry/entry';
import { EntryService } from '../entry.service';

@Component({
  moduleId: module.id,
  selector: 'my-entry-detail',
  templateUrl: 'entry-detail.component.html'
})

export class EntryDetailComponent implements OnInit {
  @Input()
  entry: Entry;

  constructor(
    private entryService: EntryService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.entryService.getEntry(+params['id']))
      .subscribe(entry => this.entry = entry);
  }

  goBack(): void {
    this.location.back();
  }

}
