import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { AppComponent }  from './app.component';
import { DashboardComponent } from './DashboardComponent/dashboard.component';
import { EntryDetailComponent } from './EntryDetailComponent/entry-detail.component';
import { EntriesComponent } from './EntriesComponent/entries.component';

import { EntryService } from './entry.service';

import { AppRoutingModule } from './app-routing.module';

@NgModule({
  imports:      [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    EntryDetailComponent,
    EntriesComponent
  ],
  providers:    [ EntryService ],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }
